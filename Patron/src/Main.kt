import factory.*

fun main() {
    val Invitado = UserFactory.getUser(UserType.Invitado, "Sebatián ", "Córdova")
    with(Invitado) {

        println(getNombreCompleto())
        println(categoria())
        println("Mostrar Pase de Entrada al Club: ${mostrarPase()}")
    }

    val Socio = UserFactory.getUser(UserType.Socio, "Marcelo", "Noboa")
    with(Socio) {
        println(getNombreCompleto())
        println(categoria())
        println("Mostrar Pase de Entrada al Club: ${mostrarPase()}")
    }
}