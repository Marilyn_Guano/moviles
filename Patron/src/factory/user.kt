package factory

interface User{
    val nombre: String
    val apellido: String

    fun getNombreCompleto()="$nombre $apellido "
    fun categoria(): String
    fun mostrarPase():Boolean

}
