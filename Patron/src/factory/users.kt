package factory

enum class UserType{ Socio, Invitado }
class Invitado(override val nombre: String,override val  apellido:String):User{
    override  fun categoria()="Invitado"
    override fun mostrarPase()= true
}

class Socio(override  val nombre: String, override val apellido: String):User{
    override fun categoria()= "Socio"
    override fun mostrarPase()= false
}