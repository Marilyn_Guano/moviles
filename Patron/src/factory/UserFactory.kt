package factory

object UserFactory {
    fun getUser(userType: UserType, nombre: String, apellido: String): User {
        return when (userType) {
            UserType.Socio -> Socio(nombre = nombre, apellido= apellido)
            UserType.Invitado -> Invitado(nombre = nombre, apellido = apellido)
        }
    }
}