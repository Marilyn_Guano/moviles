package pattern

class Wrapper : Packing {
    override fun pack(): String {
        return "Wrapper"
    }
}