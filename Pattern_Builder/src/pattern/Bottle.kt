package pattern

class Bottle : Packing {
    override fun pack(): String {
        return "Bottle"
    }
}