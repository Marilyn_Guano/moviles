package com.example.time_fighter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import io.opencensus.metrics.export.Summary

class ScoresRecycleViewAdapter:RecyclerView.Adapter<ScoreViewHolder>(){
    val db=FirebaseFirestore.getInstance()
    val players:MutableList<Player> = mutableListOf<Player>()
    init {


        //un cueri a player el snapshot contiene todos los argumentos de player de la base
        //para mostrar informaciòn en tiempo real
        val playersRef=db.collection( "players")
        playersRef
            .orderBy("score")
            .addSnapshotListener{snapshot, error->
            if (error !=null){
                return@addSnapshotListener
            }

            for(doc in snapshot!!.documentChanges){
                when(doc.type){
                    DocumentChange.Type.ADDED ->{
                        val player=Player(
                            doc.document.getString("name")!!,
                            doc.document.getDouble("score")!!.toInt()
                        )

                        players.add(player)
                        notifyItemInserted(players.size-1)
                    }
                    else ->return@addSnapshotListener
                }



            }

        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoreViewHolder {
        val view =LayoutInflater.from(parent.context).inflate(R.layout.scores_view_holder,parent,false)
        return ScoreViewHolder(view)
    }

    override fun getItemCount(): Int {
        return players.size
    }

    override fun onBindViewHolder(holder: ScoreViewHolder, position: Int) {
        holder.playerName.text=players[position].name
        holder.playerScore.text=players[position].score.toString()
    }

}