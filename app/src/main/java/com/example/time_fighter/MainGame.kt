package com.example.time_fighter

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_main_game.*
import kotlinx.android.synthetic.main.fragment_main_game.time_left as time_left1


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [main_game.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [main_game.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class main_game : Fragment() {
    // TODO: Rename and change types of parameters

    internal lateinit var welcomeMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var goButton: Button
    internal lateinit var timeLeftTextView: TextView
    //  Guadar las variables
    @State


    var score=0
    @State
    var gameStarted=false
    @State
    var timeLeft=10
    private var initialCountDown: Long = 10000
    private var countDownInterval: Long= 1000
    private lateinit var countDownTimer:CountDownTimer

    val args: main_gameArgs by navArgs()
    var db = FirebaseFirestore.getInstance()
    var name= ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        StateSaver.restoreInstanceState(this,savedInstanceState)
        gameScoreTextView = view.findViewById(R.id.score)
        // se debe iicializar
        timeLeftTextView= view.findViewById(R.id.time_left)
        goButton = view.findViewById(R.id.go_button)

        // voy a rrevisar si guarde algo // guardar el estado de las variables
        if (savedInstanceState !=null){
            score= savedInstanceState.getInt(SCORE_KEY)
        }
        gameScoreTextView.text= getString(R.string.score,score)

        welcomeMessage = view.findViewById(R.id.welcome_message)
        val playerName = args.playerName
        name=playerName
        welcomeMessage.text=playerName.toString()
        welcomeMessage.text = getString(R.string.welcome_player, playerName.toString())
        gameScoreTextView.text = getString(R.string.score, score)

        goButton.setOnClickListener {incrementScore()}
        // EN CLASES
        if (gameStarted){
            restoregame()
            return
        }


        resetGame()

    }
 // En clases
    private fun incrementScore(){
        if(!gameStarted){
            startGame()
        }
        score++
        gameScoreTextView.text= getString(R.string.score,score)

    }

    // En clases

    private  fun resetGame(){
        score=0
        gameScoreTextView.text=getString(R.string.score,score)
        timeLeft = 10
        timeLeftTextView.text=getString(R.string.time_left,timeLeft)
        countDownTimer = object : CountDownTimer(initialCountDown,countDownInterval){
            override fun onFinish() {
                endGame()
            }
         // en clases
            override fun onTick(millisUntilFinished: Long) {
              timeLeft= millisUntilFinished.toInt()/1000
                timeLeftTextView.text=getString(R.string.time_left,timeLeft)
            }
        }
        gameStarted= false
    }

    private fun startGame(){
        countDownTimer.start()
        gameStarted=true

    }
    // EN CLASES

    private fun restoregame(){
        gameScoreTextView.text= getString(R.string.score,score)
        countDownTimer=object  : CountDownTimer(
            timeLeft * 1000L, countDownInterval
        ){
            // aumentamos endGame
            override fun onFinish() {
                endGame()

            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft= millisUntilFinished.toInt() /1000
                timeLeftTextView.text= getString(R.string.time_left,timeLeft)

            }
        }

        countDownTimer.start()
    }

    // EN CLASES

    private fun endGame(){
        Toast.makeText ( activity,
            getString(R.string.end_game, score),
            Toast.LENGTH_LONG).show()


        val data = HashMap<String, Any>()
        data["name"]= name
        data["score"]=score

        db.collection("players")
            .add(data)
            .addOnSuccessListener {documentReference ->
                Log.d("Main Game", "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener{e ->
                Log.d("Main_Game", "Error adding document", e)
            }


        resetGame()
    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(SCORE_KEY, score)
        StateSaver.saveInstanceState(this,outState)

        countDownTimer.cancel()
    }





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }




    // diccionario de cosas
    /*companion object{
        private val SCORE_KEY="SCORE"
    } */

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed( uri: Uri) {

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {

        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment main_game.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            main_game().apply {
                arguments = Bundle().apply {

                }
            }

        private val SCORE_KEY="SCORE"
    }
}
